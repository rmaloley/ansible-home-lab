#!/bin/bash

# Set some variables
DRINKAPPHOME="/var/drinkapp2"

# Update our PATH
PATH=$PATH:$DRINKAPPHOME/node_modules/pm2/bin

# If our directory currently exists then delete it so we have clean files.
if [ -d "$DRINKAPPHOME" ]; then
    echo "Deleting old files from $DRINKAPPHOME."
    rm -rf $DRINKAPPHOME
fi

# Get our webapp files from bitbucket
git clone https://rmaloley@bitbucket.org/rmaloley/drinkapp2.git $DRINKAPPHOME

# Check for the existence of our .env file.
if [ ! -f /root/.env ]; then
    echo "Environment file not found. Aborting!"
    exit 1
fi

# Get our environment file.
cp /root/.env $DRINKAPPHOME

# Change our working directory
cd $DRINKAPPHOME || exit

# Install our dependencies
npm install

# Determine if our application is running. If running issue a restart. If not running (fresh install) then start it. If other then we have a problem...
PROC=$(pgrep -c node)

if [ "$PROC" -eq 0 ]; then
    # No instances running.
    pm2 start $DRINKAPPHOME/bin/www
fi
if [ "$PROC" -eq 1 ]; then
    # Restart our instance
    pm2 restart $DRINKAPPHOME/bin/www
fi
if [ "$PROC" -gt 1 ]; then
    # Something messed up and we have more than two instances - delete them and start a single instance.
    pm2 delete all
    pm2 start $DRINKAPPHOME/bin/www
fi

# Exit
exit 0