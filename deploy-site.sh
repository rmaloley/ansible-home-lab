#!/bin/bash
# File: deploy-site.sh

#
# Read a CSV of hostname, address, and host (hypervisor) and deploy some VMs.
#

# Define our input CSV. Format is simple: name,address,host
INPUT="deploy_inventory.csv"

# We will send the CSV through AWK and then xargs to sequentially run the deployment script.
cat $INPUT | awk -F "," '{print " -n "$1" -a "$2" -h "$3}' | xargs -I {} echo "./scripts/deploy-vm.sh {}" | xargs -I {} sh -c {}

# Sleep for 180s to ensure that all start up scripts and commands complete successfully.
#sleep 180s

# Call Ansible to run our site playbook :)
#ansible-playbook -i inventory-static-site site.yml
echo "Now run: ansible-playbook -i inventory-static-site site.yml"

# Exit
exit 0