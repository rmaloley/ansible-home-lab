#!/bin/bash

# Clean local log files
rm -f *.log

# Reset hosts file (removes VM01-05 entries)
sed -i '/\bvm/d' ~/.ssh/known_hosts
