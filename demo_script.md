# Demo Script #

This is the script to follow for the presentation and demonstrations.

Note: ssh -L 5901:localhost:5901 <host>

# 1 

Execute the ./deploy-site.sh script to explain what it is doing: 

* Reads in a CSV with our required machine information
* Executes a second script to deploy a virtual machine with those details
* Can begin my Ansible playbook if necessary

Review the ./scripts/deploy-vm.sh script to explain what it is doing: 

* Takes in a set of arguments: name, IP address, and host
* Assuming all arguments compute: 
    * Connects to our hypervisor and creates a new VM from template
    * Customizes the new VM with a few start up commands and a start up script
    * Powers on the VM
* Results in a VM that is configured with an appropriate IP address and is able to resolve the rest of the lab

# 2 

Execute the Ansible playbook for the site and review what it is doing: 

* The site.yml file directs ansible to configure groups of hosts with one or multiple roles.
* The common role:
    * Installs a common set of programs
    * Configures the NTP service
    * Starts the firewall service
    * Creates a new group called "admins" - anyone in this group will be able to use SUDO
    * Creates two local users - one of whom is an admin
    * Distributes a new /etc/sudoers file that enables anyone in the "admins" group to issue sudo commands
* The db-mongodb role: 
    * Configures SELinux to allow MongoDB to run
    * Installs MongoDB
    * Configures MongoDB to allow remote connections
    * Configures the firewall to allow MongoDB remote connections
    * Starts MongoDB
* The web role: 
    * Installs Node.JS which is what our webapp will run on
    * Configures the firewall
    * Deploys the webapp to /var/drinkapp
    * Starts the webapp on port 80
* The lb role: 
    * Installs haproxy
    * Installs the proper configuration to load balance our two web servers
    * Configures the firewall
    * Starts appropriate services
* The workstation role: 
    * Adds additional users to the workstation.
    * Sets a MOTD legal notice for end users.

# 3 

Demonstration Time! 

* Validation of end users on workstation.

Log in as a new user to show that we indeed are allowed to use the system and there is a message of the day.
    * nancy
    * lucy
    * tom

* NODEJS-Drinkapp

This is a Node.JS application that uses the ExpressJS Framework and EJS (Enhanced JavaScript) Rendering engine. I wrote this application following a tutorial for a similar application. What is DrinkApp? DrinkApp is a web application with a MongoDB backend that allows users to submit their favorite drinks! 

* Break a web host

In this demonstration I am going to update the firewall on vm02 and remove port 80 from the configuration. The expected result is that I am unable to access our web app on vm02. 

I can fix this by simply running Ansible: ansible-playbook -i inventory-static-site --limit 'vm02' site.yml

Validate with web browser.

*  Update and re-deploy our web application.

Lets update the DrinkApp GUI in some manner. We will modify it, commit it, and then push it to Bitbucket. Once in Bitbucket we can deploy it to a single host.

ansible-playbook -i inventory-static-site --limit 'vm02' roles/web/tasks/deploy-webapp.yml

Now we could simply deploy this to all of web hosts by running our full site configuration task.




