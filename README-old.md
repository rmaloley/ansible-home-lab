# README #

This is a demonstration and challenge to quickly spin up a static lab of five VMs. 
The ansible playbooks and shell scripts are meant to be reusable in a variety of situations, however these are highly specific to my environment.

# REQUIREMENTS #

Again these plays and scripts are highly specific to my environment. As such: 

* A CentOS 7 Virtualization Host running libvirtd/KVM
* SSH Key Authentication for the entire environment
* The user/server that you execute code from must, via SSH Keys, become root on all others servers such as the virtualization host
* Deployment scripts configure pre-assigned static addresses. Ensure these are either in a DNS Server already OR utilize a managed /etc/hosts file.
	* I will be using a managed hosts file for the purpose of this demonstration.
* You need a CentOS 7 template available on the virtualization host. 
* Have fun? 

# Files to know about # 

1. inventory-static-site: This is our ansible inventory file for the environment. It breaks up our inventory into groups.
2. deploy-site.sh: This is our shell script that will: 
	1. Provision our virtual machines (by calling scripts/deploy-vm.sh)
		* Configure your input file (csv) to match inventoy-static-site.
	2. Kick off the ansible playbook to configure said environment
3. scripts/deploy-vm.sh: This is our vm provisioning/deployment script. It takes arguments (-n name, -a address, -h host) to determine the vm name, IP address, and the destination virtualization host.
	* You can configure global variables in this script to match your environment. Specifically you can configure your gateway, dns, dns search, template, and disk path for virtual machines.
4. deploy_inventory.csv: This is our csv used by deploy-site.sh. This file should match what will be in inventory-static-site. Otherwise weird stuff may happen...
	* This is a reusable file for deploy-site.sh to automatically deploy more than one VM.



### Ansible playbooks? ###

* For deploying and then managing a small static environment within my home lab.

### The Plan ###

The plan is simple: 

1. Create a CentOS 7 Template (done)
2. Create a shell script OR playbook (or combination that will: 
	* Deploy a new VM from the template (done)
	* Update the VM IP Address/Hostname (done)
	* Inject additional required environment information such as SSH Authorized Keys and an appropriate /etc/hosts file. (done)
3. Create/maintain a "common" role playbook that applies to the entire site and does the following: (done)
	* Configures NTP (done)
	* Configures SUDO (done)
	* Installs common software (git, python, perl, vim, mutt) (done)
	* Adds some sample users: (done)
		* josh - member of "admins" - can utilize sudo! 
		* bob - just a regular user. NO SUDO!
4. A web playbook that deploys an appropriate web service and sample content to two servers.
	* Configure firewall
	* Deploy sample application...
5. A db playbook that deploys a simple database with basic content accessible by both web servers.
	* Install a database server (maybe mongodb to learn something new?) (done)
	* Enable the database services (done)
	* Configure the firewall (done)
	* Configure for remote access (done)
	* Configure SELinux... permissive mode in this case. Thanks Mongo... (done)
6. A lb (loadbalancer) playbook that deploys haproxyd and a simple configuration to load balance traffic across both web servers.
	* Install loadbalancer
	* Enable services
	* Configure firewall
	* Deploy configuration for the two web servers
7. A workstation playbook that deploys appropriate packages and configurations to a workstation or client used to access the lab services.
	* Installs common packages
	* I should be able to browse my basic website with a text based browser...
	* Also has a custom MOTD! 
8. SAMPLE APPLICATION
	* Written in Node, Express, and EJS
	* Allows you to submit your favorite drinks to a database
	* Runs on its' own web server

All DONE! 

