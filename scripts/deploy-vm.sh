#!/bin/bash
# File: deploy-vm.sh

#
# This script will automate the deployment of new VMs. 
#

# Input arguments: hostname and ip and hypervisor host.

NAME=
IPADDR=
HOST=

# Global Variables
#
TEMPLATE="centos7-template-01"
DISKPATH="/var/lib/libvirt/images/"
GW="192.168.1.1"
DNS1="192.168.1.7"
DNS_SEARCH="remote.rrcomputerconsulting.com"
LOGFILE=

# Function: usage
#   Print the usage instructions.
function usage
{
    echo "deploy-vm.sh is a simple shell script that helps me deploy virtual machines in my home lab."
    echo "Please supply the following arguments:"
    echo "     -n : The name of the new virtual machine."
    echo "     -a : The IP address of the new virtual machine."
    echo "     -h : The hypervisor host name or FQDN."
    echo "     -? : This help page."
    exit 0
}

# Logic to read our arguments and perform an action.

while getopts :n:a:h: option
do 
    case "${option}" in
        n)  NAME=$(echo "${OPTARG}" | awk '{print tolower($0)}')
            LOGFILE=$NAME.log
            ;;
        a)  IPADDR=${OPTARG}
            ;;
        h)  HOST=$(echo "${OPTARG}" | awk '{print tolower($0)}')
            # Set URI now that we have required inputs. 
            URI="qemu+ssh://root@$HOST/system"
            ;;
        *)  usage
            exit 1
            ;;
    esac
done

# Check required inputs.
if [ -z "$HOST" ]
    then
        echo "You must provide the target hypervisor host for the guest virtual machine. Run -? for help."
        exit 1
fi
if [ -z "$IPADDR" ]
    then
        echo "You must provide an IP address for the guest virtual machine. Run -? for help."
        exit 1
fi
if [ -z "$NAME" ]
    then
        echo "You must provide a name for the guest virtual machine. Run -? for help."
        exit 1
fi

# IF our log file exists destroy it
if [ -a $LOGFILE ]
    then
        rm -f $LOGFILE
fi



# Status Update
echo $(date) "# Cloning from template $TEMPLATE..." >> $LOGFILE 2>&1

# Clone from template
virt-clone --connect $URI --original $TEMPLATE --name $NAME --file $DISKPATH/$NAME.qcow2 >> $LOGFILE 2>&1

# Status Update
echo $(date) "# Building bootstrap file..." >> $LOGFILE 2>&1

# Build our firstboot bootstrap script
#   Set hostname
#   Execute nmcli commands to configure network at firstboot
if [ -a bootstrap.sh ]
    then
        rm -f bootstrap.sh
fi
echo "#!/bin/bash" >> bootstrap.sh
echo "nmcli con mod eth0 ip4 $IPADDR/24 gw4 $GW ipv4.method manual ipv4.dns $DNS1 ipv4.dns-search $DNS_SEARCH" >> bootstrap.sh
echo "nmcli con up eth0" >> bootstrap.sh

# Status Update
echo $(date) "# Sending the bootstrap file to $HOST..." >> $LOGFILE 2>&1

# Copy the bootstrap.sh file to the hypervisor host.
scp bootstrap.sh root@$HOST:/tmp/bootstrap.sh >> $LOGFILE 2>&1

# Status Update
echo $(date) "# Sending the managed /etc/hosts file to $HOST..." >> $LOGFILE 2>&1

# Copy the hosts file to the hypervisor host for insertion into virt-customize.
scp files/hosts root@$HOST:/tmp/hosts >> $LOGFILE 2>&1

# Status Update
echo $(date) "# Sending the initial /root/.ssh/authorized_keys file to $HOST..." >> $LOGFILE 2>&1

# Copy the default authorizedkeys file to the hypervisor host for insertion into virt-customize.
scp files/authorized_keys root@$HOST:/tmp/authorized_keys >> $LOGFILE 2>&1

# Status Update
echo $(date) "# Customizing $NAME..." >> $LOGFILE 2>&1

# virt-customize to set hostname and inject firstbook script and firstboot commands.
## Set hostname to $NAME
## Configure firstboot command to create ssh keypair
## Configure firstboot script which brings up the network interface
## Configure firstboot command to to copy the initial authorized_keys file from the local /tmp directory to the root/.ssh directory
## Copy the /etc/hosts file into the virtual machine guest
## Copy the authorized_keys file into the virtual machine guest /tmp/authorized_keys location *** This is because the /root/.ssh directory does not exist until the keypair is generated.
# Build a virt-cust input file...
if [ -a virt-cust ]
    then
        rm -f virt-cust
fi
echo "virt-customize -d $NAME --hostname $NAME --firstboot-command 'ssh-keygen -f /root/.ssh/id_rsa -q -N "\"\""' --firstboot /tmp/bootstrap.sh --firstboot-command 'cp /tmp/authorized_keys /root/.ssh/' --copy-in /tmp/hosts:/etc --copy-in /tmp/authorized_keys:/tmp" >> virt-cust

# Execute SSH remote commands
ssh -T root@$HOST < virt-cust >> $LOGFILE 2>&1

# Status Update
echo $(date) "# Starting $NAME on $HOST..." >> $LOGFILE 2>&1

# Start the virtual machine
virsh -c $URI start $NAME >> $LOGFILE 2>&1

# Status Update
echo $(date) "# Cleaning files..." >> $LOGFILE 2>&1

# Clean up files
ssh -T root@$HOST 'rm -f /tmp/bootstrap.sh' >> $LOGFILE 2>&1
ssh -T root@$HOST 'rm -f /tmp/authorized_keys' >> $LOGFILE 2>&1
ssh -T root@$HOST 'rm -f /tmp/hosts' >> $LOGFILE 2>&1
rm -f bootstrap.sh >> $LOGFILE 2>&1
rm -f virt-cust >> $LOGFILE 2>&1

# Exit the script successfully.
echo $(date) "Done provisioning $NAME on $HOST." >> $LOGFILE 2>&1
exit 0